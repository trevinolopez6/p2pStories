import React from 'react'
import { StyleSheet, TouchableHighlight, View } from 'react-native'

export default ({ action }) => {
  return (
    <TouchableHighlight onPress={action} style={styles.wrapper}>
      <View style={styles.container}>
        <View style={styles.record} />
      </View>
    </TouchableHighlight>
  )
}
var styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    zIndex: 999,
    bottom: '5%',
    right: '5%'
  },
  container: {
    height: 70,
    width: 70,
    backgroundColor: 'black',
    borderRadius: 70,
    alignItems: 'center',
    justifyContent: 'center'
  },
  record: {
    height: 35,
    width: 35,
    backgroundColor: 'red',
    borderRadius: 35
  }
})
